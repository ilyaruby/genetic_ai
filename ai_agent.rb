class AiAgent
  ProgSize = 256
  #CRYSTAL: @prog: Array(Int32)
  #CRYSTAL: @prog_orig: Array(Int32)
  #CRYSTAL: @steps_remaining : Int32
  def initialize(p, max_steps: 256, debug: false)
    @prog_orig = p.dup
    @prog = p.dup
    @steps_remaining = max_steps
    @next_address = 0
    @debug = debug
    @max_steps = max_steps
    log "Initializing, steps: #{max_steps}"
  end

  def prog
    @prog_orig
  end

  def reset_steps
    @steps_remaining = @max_steps
    reset_agent
  end

  def reset_agent
    @prog = @prog_orig.dup
  end

  def log(message)
    return unless @debug
    puts message
  end

  def continue
    result = run_operation_at(@next_address)
    while result.state == :continue
      @next_address = result.address
      log "next: #{@next_address}"
      result = run_operation_at @next_address
    end
    @next_address = ( @next_address + 4 ) % 4
    return result.state
  end

  def to_s
    @prog.map do |i|
      i.to_s(36)
    end.join
  end

  def mutate_prog
    step_to_mutate = rand(0..(ProgSize-1))
    @prog_orig[step_to_mutate] = rand(0..255)
  end

  private

  def run_operation_at(start_address)
    log "old: #{@prog}"
    return OperationResultState.new(:max_step, 0) if out_of_steps?

    a, b, c, d = read_registers_at start_address
    log "abcd: #{[a, b, c, d]}"

    return OperationResultState.new(:input, 0) if a == 0
    return OperationResultState.new(:output, 0) if a == 1

    log "writing #{read_at(a)} + #{read_at(b)} to #{b}"
    write_to b, read_at(a) + read_at(b)

    log "new: #{@prog}"
    if read_at(b) < ProgSize / 2
      return OperationResultState.new(:continue, c)
    else
      return OperationResultState.new(:continue, d)
    end
  end 
  
  def read_at(address)
    @prog[address % ProgSize]
  end

  def read_registers_at(start_address)
    a = read_at start_address
    b = read_at start_address + 1
    c = read_at start_address + 2
    d = read_at start_address + 3
    [a, b, c, d]
  end

  def write_to(address, value)
    @prog[address % ProgSize] = value % 256
  end

  def out_of_steps?
    log "Steps remaining: #{@steps_remaining}"
    is_zero = (@steps_remaining == 0)
    @steps_remaining -= 1
    is_zero
  end
end

