require "crsfml"

class GraphArea
  getter :draw_area_x
  getter :draw_area_y
  @window : SF::RenderWindow
  @title_color : SF::Color
  @title : String
  @x : Int32
  @y : Int32
  @draw_area_x : Int32
  @draw_area_y : Int32
  def initialize(window, title, x, y)
    @window = window
    @size = 256
    @title_color = SF::Color::Red
    @title_font_size = 12
    @title = title
    @x = x
    @y = y
    @draw_area_x = @x
    @draw_area_y = @y + (@title_font_size * 2)
    @font = SF::Font.from_file("/usr/share/fonts/TTF/DejaVuSans.ttf")
  end
  def clear
    blackbox = SF::RectangleShape.new(SF.vector2(@size, @size + (@title_font_size * 2)))
    blackbox.fill_color = SF::Color.new(32, 32, 32)
    blackbox.move(SF.vector2(@x, @y))
    @window.draw blackbox
  end
  def draw_title
    text = SF::Text.new
    text.font = @font
    text.string = @title
    text.character_size = @title_font_size
    text.color = @title_color
    text.move(SF.vector2(@x, @y))
    @window.draw(text)
  end
  def draw_in_area
    yield(self)
  end
end

class DatasetGrapher
  getter :area1, :area2, :area3, :area4, :area5, :area6
  def initialize 
    @window = SF::RenderWindow.new(
      SF::VideoMode.new(1000, 800), "Graph",
      settings: SF::ContextSettings.new(depth: 24, antialiasing: 0)
    )
    @window.vertical_sync_enabled = false
    @area1 = GraphArea.new(@window, "Current fitness", 0, 0)
    @area2 = GraphArea.new(@window, "Fitness average median (256 rounds) * 16", 300, 0)
    @area3 = GraphArea.new(@window, "Fitness upper quadrant fit (256 rounds)", 0, 300)
    @area4 = GraphArea.new(@window, "Population winners count (256 rounds)", 300, 300)
    @area5 = GraphArea.new(@window, "Program", 600, 0)
    @area6 = GraphArea.new(@window, "Generations", 600, 300)
  end
  def draw_dataset(dataset, start_x, start_y)
    (0..(dataset.size-1)).each do |x|
      line = SF::RectangleShape.new(SF.vector2(1, dataset[x]))
      line.move(SF.vector2(start_x + x, start_y))
      @window.draw line
    end
  end
  def draw_array(*args)
    draw_array_generic(*args) do |cell|
      make_color_fast(cell)
    end
  end
  def draw_array_log2(*args)
    draw_array_generic(*args) do |cell|
      make_color_fast(log2(cell))
    end
  end
  def draw_array_grayscale(*args)
    draw_array_generic(*args) do |cell|
      make_color_grayscale(cell)
    end
  end
  def draw_array_generic(array, start_x, start_y)
    pix_size = 8
    (0..15).each do |x|
      (0..15).each do |y|
        cell = array[y * 8 + x]
        pixel = SF::RectangleShape.new(SF.vector2(pix_size, pix_size))
        pixel.fill_color = yield(cell)
        pixel.move(SF.vector2(start_x + x * pix_size, start_y + y * pix_size))
        @window.draw pixel
      end
    end
  end
  def make_color_fast(cell)
    r = cell.to_i % 64 * 64
    g = cell.to_i % 32 * 32
    b = cell.to_i % 16 * 16
    SF::Color.new(r, g, b)
  end
  def make_color_grayscale(cell)
    r = cell < 255 ? cell : 255
    g = cell < 255 ? cell : 255
    b = cell < 255 ? cell : 255
    SF::Color.new(r, g, b)
  end
  def draw_line(line, start_x, start_y)
    line = SF::RectangleShape.new(SF.vector2(1, line))
    line.move(SF.vector2(start_x, start_y))
    @window.draw line
  end
  def clear
    @window.clear SF::Color::Black
  end
  def draw_cycle
    return false unless @window.open?
    while event = @window.poll_event()
      @window.close if event.is_a? SF::Event::Closed
    end

    yield(self)
  end
  def display
    @window.display
  end
  def log2(pos)
		level = 0
		until pos < 2
			level += 1
			pos /= 2
		end
		level
	end
end

