class AiAgentFitness
  getter :agent, :result, :fitness, :generation
 #RUBY-REPLACE-BEFORE:   attr_reader :agent, :result, :fitness, :generation
  @result : Symbol #RUBY-COMMENT-IT
  prog : Array(Int32) #RUBY-COMMENT-IT
  @fitness : Float64 #RUBY-COMMENT-IT
  def initialize(prog, generation = 0)
    @generation = generation
    @fitness = 0.0
    @agent = AiAgent.new prog
    @game = Game.new
    @next_instruction_at = 0

    @result = :start
    @last_correct = false
    @reads = 0
    @writes = 0
    @last_io_was_read = false
  end
  def run_agent
    while should_continue?
      run_agent_once
      make_io
      calculate_score
    end
  end
  def mutate_prog
    @agent.mutate_prog
  end
  def prog
    @agent.prog
  end
  def reset
    @agent.reset_steps
    @last_is_read = false
    @reads = 0
    @writes = 0
    @last_correct = false
    @result = :start
  end


  def make_io
    read_from_game if is_reading?
    write_to_game if is_writing?
  end
  def read_from_game
    @agent.prog[0] = @game.given
    @reads += 1
    @last_io_was_read = true
  end
  def write_to_game
    @last_correct = @game.is_correct?(@agent.prog[1])
    @writes += 1
    @last_io_was_read = false
  end
  def is_reading?
    result == :input
  end
  def is_writing?
    result == :output
  end
  def run_agent_once
    @result = @agent.continue
    #puts "Agent: #{@agent.hash} result #{@result}"
  end
  def should_continue?
    ! ended?
  end
  def ended?
    result == :max_step
  end
  def should_be_reading?
    return true if @reads == 0
    return false if @last_io_was_read
    true
  end
  def should_be_writing?
    return true if @last_io_was_read
    false
  end
  def inc_max_255(x, val)
    x + val > 255.0 ? 255.0 : x + val
  end
  def dec_min_0(x, val)
    x - val < 0.0 ? 0.0 : x - val
  end
  def calculate_score
    if is_reading?
      if should_be_reading?
        @fitness = inc_max_255(@fitness, 1.0)
      #else
        #@fitness = dec_min_0(@fitness, 0.02)
      end
    end
    #if is_writing?
    #  if should_be_writing?
    #    @fitness = inc_max_255(@fitness, 1)
    #  else
    #    @fitness = dec_min_0(@fitness, 0.02)
    #  end
    #end
    #
    #@fitness += 1 if is_reading? && @reads == 1 && @writes == 0
    #@fitness += 1 if is_writing? && @reads == 1 && @writes == 1
    #@fitness += 1 if @fitness == 2 && @last_correct
  end
end