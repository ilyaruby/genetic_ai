class OperationResultState
  @state : Symbol #RUBY-COMMENT-IT
  @address : Int32 #RUBY-COMMENT-IT
  getter :state, :address
 #RUBY-REPLACE-BEFORE:   attr_reader :state, :address
  def initialize(state, address)
    @state = state
    @address = address
  end 
end