class AiAgent
  ProgSize = 256
  @prog: Array(Int32) #RUBY-COMMENT-IT
  @prog_orig: Array(Int32) #RUBY-COMMENT-IT
  @steps_remaining : Int32 #RUBY-COMMENT-IT
  def initialize(p)
    @prog_orig = p.dup
    @prog = p.dup
    @steps_remaining = 256
    @next_address = 0
  end

  def prog
    @prog_orig
  end

  def reset_steps
    @steps_remaining = 256
    reset_agent
  end

  def reset_agent
    @prog = @prog_orig.dup
  end

  def continue
    result = run_operation_at(@next_address)
    while result.state == :continue
      @next_address = result.address
      result = run_operation_at @next_address
    end
    @next_address = ( @next_address + 4 ) % 4
    return result.state
  end

  def to_s
    @prog.map do |i|
      i.to_s(36)
    end.join
  end

  def mutate_prog
    step_to_mutate = rand(0..(ProgSize-1))
    @prog_orig[step_to_mutate] = rand(0..255)
  end


  def run_operation_at(start_address)
    return OperationResultState.new(:max_step, 0) if out_of_steps?

    a, b, c, d = read_registers_at start_address

    return OperationResultState.new(:input, 0) if a == 0
    return OperationResultState.new(:output, 0) if a == 1

    write_to b, read_at(a) + read_at(b)

    if read_at(b) < ProgSize / 2
      return OperationResultState.new(:continue, c)
    else
      return OperationResultState.new(:continue, d)
    end
  end 
  
  def read_at(address)
    @prog[address % ProgSize]
  end

  def read_registers_at(start_address)
    a = read_at start_address
    b = read_at start_address + 1
    c = read_at start_address + 2
    d = read_at start_address + 3
    [a, b, c, d]
  end

  def write_to(address, value)
    @prog[address % ProgSize] = value % 256
  end

  def out_of_steps?
    #puts @steps_remaining
    @steps_remaining -= 1
    @steps_remaining == 0
  end
end