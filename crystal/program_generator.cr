class ProgramGenerator
  r : Bytes #RUBY-COMMENT-IT
  def self.random(prog_size)
    Array.new(prog_size) { Kernel.rand(0..255) }
  end
end