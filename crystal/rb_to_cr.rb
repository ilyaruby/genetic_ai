#!/usr/bin/env ruby

def process(source_lines)
  new_source = []
  should_skip_next = false
  source_lines.each_with_index do |line_original, i|
    line = LineProcessed.new(line_original)
    if line.should_uncomment?
      puts "UNCOMMENTED"
      new_source << line.clean + ' #RUBY-COMMENT-IT'
    elsif line.should_replace_next?
      puts "REPLACE"
      new_source << line.clean
      should_skip_next = true
    else
      if should_skip_next
        new_source << " #RUBY-REPLACE-BEFORE: " + line_original
        should_skip_next = false
      else
        new_source << line.original
      end
    end
  end
  new_source.map do |line|
    if line =~ /\(&:/
      line.gsub('(&:', '(&.')
    elsif line.strip == 'private'
      nil
    else
      line
    end
  end.reject(&:nil?).join("\n")
end

class LineProcessed
  def initialize(orig_line)
    @orig_line = orig_line
  end
  def original
    @orig_line
  end
  def extract_parts
    @orig_line.match(/(.*)(#CRYSTAL[^:]*): (.*)/).to_a.drop(1)
    #puts r.inspect
    #r
  end
  def should_uncomment?
    directive == '#CRYSTAL'
  end
  def should_replace_next?
    directive == '#CRYSTAL-REPLACE-NEXT'
  end
  def left
    @left ||= extract_parts[0]
  end
  def right
    @right ||= extract_parts[2]
  end
  def directive
    @directive ||= extract_parts[1]
  end
  def clean
    left + right
  end
end

ruby_file_name = ARGV[0]
exit 1 unless ruby_file_name
ruby_source = File.read ruby_file_name
ruby_lines = ruby_source.split("\n")
puts "File #{ruby_file_name} has #{ruby_source.size} lines"
crystal_file_name = ruby_file_name.gsub(/.rb\Z/, '.cr').gsub(/\A\./, '')
File.write(crystal_file_name, process(ruby_lines))
puts "Covnerted to #{crystal_file_name}"

