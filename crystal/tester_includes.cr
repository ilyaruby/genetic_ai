module TesterIncludes
  def print_stats
    #fitness_max = @population.map(&.fitness).max
    #good = @population.select {|agent| agent.fitness == fitness_max}
    #print "#{fitness_max} "
		elapsed = Time.now - @start_time_overall
		puts "Elapsed: #{elapsed.to_i} seconds, speed: #{@iteration_real.to_f / elapsed.to_f} iter/sec"
		return

    @dg.draw_cycle do |d|
      d.area1.draw_in_area do |da|
        da.clear
        da.draw_title
        d.draw_array(@population.map(&.fitness), da.draw_area_x, da.draw_area_y)
      end if (@iteration % 2) == 0
      d.area2.draw_in_area do |da|
        da.clear if @iteration == 0
        da.draw_title if @iteration == 0
        d.draw_line(@population_stats.fit_median, (@iteration + da.draw_area_x), da.draw_area_y)
      end
      d.area3.draw_in_area do |da|
        da.clear if @iteration == 0
        da.draw_title if @iteration == 0
        d.draw_line(@population_stats.count_upper_fit_quadrant, (@iteration + da.draw_area_x), da.draw_area_y)
      end
      d.area4.draw_in_area do |da|
        da.clear if @iteration == 0
        da.draw_title if @iteration == 0
        d.draw_line(@population_stats.count_max_fit, (@iteration + da.draw_area_x), da.draw_area_y)
      end
      d.area5.draw_in_area do |da|
        da.clear
        da.draw_title
        d.draw_array(@population.first.agent.prog, da.draw_area_x, da.draw_area_y)
      end if (@iteration % 2) == 0
      d.area6.draw_in_area do |da|
        da.clear if @iteration == 0
        da.draw_title if @iteration == 0
        d.draw_array_log2(@population.map(&.generation), da.draw_area_x, da.draw_area_y)
      end if (@iteration % 2) == 0
      d.display
    end
  end
end
