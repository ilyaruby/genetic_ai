class Game
  @given : Int32 #RUBY-COMMENT-IT
  def initialize(minimum = 0, maximum = 255)
    @minimum = minimum
    @maximum = maximum
    @median = (@minimum + @maximum / 2)
    @given = Kernel.rand(@minimum..@maximum)
  end
  def given
    @given = Kernel.rand(@minimum..@maximum)
  end
  def is_correct?(answer)
    is_really_correct? answer
  end
  def is_really_correct?(answer)
    return true if (@given < @median) && (answer < @median)
    return true if (@given >= @median) && (answer >= @median)
    false
  end
end