require 'spec_helper'

RSpec.describe ProgramGenerator do
  subject { ProgramGenerator }
  describe 'program generator' do
    let(:prog_size) { 255 }
    let(:random_value) { 63 }
    it 'generates a random start program' do
      #set a predefined 'random' number
      allow(Kernel).to receive(:rand).and_return(random_value)

      expect(subject.random(prog_size)).to eq ([random_value] * prog_size)
    end
  end
end
