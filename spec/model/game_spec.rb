require 'spec_helper'

RSpec.describe Game do
  subject { Game.new }
  describe 'gameplay' do
    let(:min) { 0 }
    let(:max) { 255 }
    let(:median) { ( min + max ) / 2}
    it 'returns a correct answer for min' do
      #set a predefined 'random' number to guess eq min
      allow(Kernel).to receive(:rand).and_return(min)

      expect(subject.is_correct?(min)).to be true
      expect(subject.is_correct?(max)).to be false
      expect(subject.is_correct?(median)).to be false
    end
    it 'returns a correct answer for max' do
      #set a predefined 'random' number to guess eq max
      allow(Kernel).to receive(:rand).and_return(max)

      expect(subject.is_correct?(min)).to be false
      expect(subject.is_correct?(max)).to be true
      expect(subject.is_correct?(median)).to be true
    end
  end
end
