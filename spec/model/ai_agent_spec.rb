require 'spec_helper'

RSpec.describe AiAgent do
  module AiAgentTest
    attr_reader :steps_remaining
    def current_prog
      @prog
    end
  end

  subject do
    class AiAgent
      include AiAgentTest
    end

    AiAgent.new(start_prog, max_steps: max_steps, debug: false)
  end

  describe 'creation' do
    let(:max_steps) { 3 }
    let(:start_prog) { [4, 5, 0, 0, 0, 0] } #just cycles

    it 'creates AiAgent instance with a correct prog' do
      expect(subject.prog).to eq start_prog
      expect(subject.current_prog).to eq start_prog
    end

    context 'cycling program' do
      # a = at(2), b = at(3), sum = 0 + 0, write to 2
      let(:start_prog) { [4, 5, 0, 0, 0, 0] } #just cycles

      it 'runs cycling program and returns max step' do
        expect(subject.continue).to eq :max_step
      end
    end

    context 'val at b alteration' do
      let(:max_steps) { 1 }
      let(:a) { 4 }
      let(:val_at_a) { 3 }
      let(:b) { 5 }
      let(:val_at_b) { 0 }
      let(:start_prog) { [a, b, 0, 0, val_at_a, val_at_b] }
      let(:expected_final_val_at_b) { val_at_a + val_at_b }
      let(:expected_final_prog) { [a, b, 0, 0, val_at_a, expected_final_val_at_b] }

      it 'alters b register' do
        subject.continue
        expect(subject.current_prog).to eq expected_final_prog
      end

      context 'alters b register second time' do
        let(:max_steps) { 2 }
        let(:expected_final_val_at_b) { val_at_b + val_at_a + val_at_a }

        it 'alters b register' do
          subject.continue
          expect(subject.current_prog).to eq expected_final_prog
        end
      end
    end

    context 'agent reset' do
      let(:max_steps) { 1 }
      let(:start_prog) { [4, 5, 0, 0, 3, 0] } # adds #4 to #5 every cycle
      let(:end_prog) { [4, 5, 0, 0, 3, 3] } # after 1 cycle

      it 'resets program' do
        expect(subject.current_prog).to eq start_prog
        subject.continue
        expect(subject.current_prog).to eq end_prog
        subject.reset_agent
        expect(subject.current_prog).to eq start_prog
      end

      it 'resets steps' do
        expect(subject.steps_remaining).to eq max_steps
        subject.continue
        expect(subject.steps_remaining).to eq -1
        subject.reset_steps
        expect(subject.steps_remaining).to eq max_steps
      end
    end
  end
end
