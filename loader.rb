require_relative "program_generator"
require_relative "tester"
require_relative "ai_agent"
require_relative "ai_agent_fitness"
require_relative "game"
require_relative "operation_result_state"
require_relative "config"

AiConfig.instance.load_config('config/ai.yml')
