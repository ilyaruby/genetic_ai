class ProgramGenerator
  #CRYSTAL: r : Bytes
  def self.random(prog_size)
    Array.new(prog_size) { Kernel.rand(0..255) }
  end
end


