class OperationResultState
  #CRYSTAL: @state : Symbol
  #CRYSTAL: @address : Int32
  #CRYSTAL-REPLACE-NEXT: getter :state, :address
  attr_reader :state, :address
  def initialize(state, address)
    @state = state
    @address = address
  end 
end


