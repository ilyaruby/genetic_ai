class PopulationStats
  #CRYSTAL-REPLACE-NEXT: property :fit_max, :fit_median, :count_upper_fit_quadrant, :count_max_fit
  attr_reader :fit_max, :fit_median, :count_upper_fit_quadrant, :count_max_fit
  #CRYSTAL: @fit_max : Float64
  #CRYSTAL: @fit_median : Float64
  #CRYSTAL: @count_upper_fit_quadrant : Int32
  #CRYSTAL: @count_max_fit : Int32
  def initialize(population)
    fitnesses = population.map(&:fitness)
    @fit_max = fitnesses.max
    #@fit_median = fitnesses.sort[fitnesses.size / 2]
    #CRYSTAL-REPLACE-NEXT: @fit_median = fitnesses.map(&.to_i).reduce(&.+) / fitnesses.size.to_f * 16.0
    @fit_median = fitnesses.map(&:to_i).reduce(&:+) / fitnesses.size.to_f * 16.0
    @count_upper_fit_quadrant = fitnesses.select {|f| f > @fit_median}.size
    @count_max_fit = fitnesses.select {|f| f == @fit_max}.size
  end
end

#CRYSTAL: require "./tester_includes"

class Tester
  PopulationSize = 256
  BestAmount = 12
  MutantsPerParent = 7 # was 15
  RandomAgentsEachRound = PopulationSize - (BestAmount * (1 + MutantsPerParent))
  POPULATION_DATA_FILENAME = "data/population.dat"
  #CRYSTAL: include TesterIncludes
  #CRYSTAL: @population : Array(AiAgentFitness)
  #CRYSTAL: @population_stats : PopulationStats
  #CRYSTAL: @iteration : Int32
  #CRYSTAL: @iteration_real : Int32
  def initialize
    #CRYSTAL: @dg = DatasetGrapher.new
		@start_time_overall = Time.now
    #CRYSTAL-REPLACE-NEXT: @population = [] of AiAgentFitness
    @population = []
    @population_stats = PopulationStats.new create_new_agents 256 # just for crystal
    @iteration = 0
    @iteration_real = 0
    #load_population
    if @population.size == 0
      @population = create_new_agents PopulationSize
      @population_stats = PopulationStats.new create_new_agents 256 # just for crystal
    end
  end
  def load_population
    File.open(POPULATION_DATA_FILENAME) do |f|
      #CRYSTAL-REPLACE-NEXT: prog = [] of Int32
      prog = []
      f.each_byte do |byte|
        prog << byte.to_i
        puts byte.to_s
      end
      if (prog.size == 256)
        @population << AiAgentFitness.new(prog)
        #CRYSTAL-REPLACE-NEXT: prog = [] of Int32
        prog = []
      end
    end
    puts "Loaded #{@population.size} agents"
  end
  def initialize_population
    @population_stats = PopulationStats.new create_new_agents 256
  end
  def test_population(iterations = nil)
    end_iteration = iterations || Float32::INFINITY
    start_time = Time.now
    (1..end_iteration).each do |iteration_real|
      @iteration_real = iteration_real
      if iteration_real % 256 == 0
        t = Time.now
        iteration_block = iteration_real / 256
        puts ""
        puts "Iteration #{iteration_real}, round: #{Time.now - t} seconds, elapsed: #{Time.now - start_time}"
      end
      @iteration = iteration_real % 256
      test_population_once
    end
  end
  def test_population_once
    run_agents
    @population_stats = PopulationStats.new @population
    print_stats
    #save_progress if @iteration_real % 256 == 0
    mutate_population
    reset_agents
  end

  private

  def save_progress
    File.open(POPULATION_DATA_FILENAME, "w") do |f|
      @population.each do |agent_fitness|
        #RYSTAL-REPLACE-NEXT: agent_fitness.prog.each {|i| b_ptr = Pointer(Void).new(i.object_id); f.write_byte(b_ptr.as(UInt8))}
        #CRYSTAL-REPLACE-NEXT: next
        f.write agent_fitness.prog.pack("c*")
      end
    end
    puts "Saved"
  end

  #CRYSTAL-REPLACE-NEXT: def print_stats_ruby
  def print_stats
    fitness_max = @population.map(&:fitness).max
    good = @population.select {|agent| agent.fitness == fitness_max}
    #print "#{fitness_max} "
  end

  def reset_agents
    @population.each(&:reset)
  end

  def create_new_agents(count)
    Array.new(count) { AiAgentFitness.new(ProgramGenerator.random(256), 0) }
  end

  def mutate_population
    best = @population.sort { |a1, a2| a1.fitness <=> a2.fitness}.last(BestAmount)

    #CRYSTAL-REPLACE-NEXT: @population = [] of AiAgentFitness
    @population = []
    best.each do |parent|
      @population << recreate(parent)
      MutantsPerParent.times do
        @population << mutate(parent)
      end
    end
    @population += create_new_agents(RandomAgentsEachRound)
  end

  def recreate(parent)
    parent.mutate_prog
    AiAgentFitness.new parent.prog, parent.generation + 1
  end

  def mutate(parent)
    parent.mutate_prog
    AiAgentFitness.new parent.prog, parent.generation + 1
  end
  
  def run_agents
    @population.each(&:run_agent)
  end
end
