require 'singleton'
require 'yaml'

class AiConfig
  include Singleton
  def load_config(file)
    @config = YAML.load_file(file)
    puts "Config file loaded: #{file}, it has #{@config.size} keys"
  end
  def [](key)
    @config[key]
  end
end
